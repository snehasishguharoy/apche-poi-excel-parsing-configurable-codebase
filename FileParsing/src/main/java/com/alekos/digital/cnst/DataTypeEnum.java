package com.alekos.digital.cnst;

public enum DataTypeEnum {
	DOUBLE("Double"), INTEGER("Integer"), STRING("String"), DATE("Date"), BYTE("byte[]");
	final String typValue;

	private DataTypeEnum(final String typValue) {
		this.typValue = typValue;
	}

	public String getName() {
		return name();
	}

	public String getValue() {
		return typValue;
	}

	@Override
	public String toString() {
		return name();
	}

}
