package com.alekos.digital.cnst;

public enum ExcelSectionEnum {
	PHYSICS("Physics");

	final String typVal;

	private ExcelSectionEnum(final String typValue) {
		this.typVal = typValue;
	}

	public String getName() {
		return name();
	}

	public String getValue() {
		return typVal;
	}

	@Override
	public String toString() {
		return name();
	}
}
