package com.alekos.digital.mapper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.alekos.digital.cnst.DataTypeEnum;
import com.alekos.digital.config.ExcelValueCnfg;

public class ExcelPojoMapper {

	public static <T> List<T> getPojos(List<ExcelValueCnfg[]> excelValueConfigs, Class<T> clazz) {
		List<T> list = new ArrayList<>();
		excelValueConfigs.forEach(evc -> {
			T t = null;
			try {
				t = clazz.getConstructor().newInstance();

			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException excp) {
				excp.printStackTrace();
			}
			Class<? extends Object> classz = t.getClass();
			for (int count = 0; count < evc.length; count++) {
				for (Field field : classz.getDeclaredFields()) {
					field.setAccessible(true);
					if (evc[count].getPojoProperty().equalsIgnoreCase(field.getName())) {

						try {
							if (DataTypeEnum.STRING.getValue().equalsIgnoreCase(evc[count].getExlColType())) {
								field.set(t, evc[count].getExlValue());
							} else if (DataTypeEnum.INTEGER.getValue().equalsIgnoreCase(evc[count].getExlColType())) {
								field.set(t, Double.valueOf(evc[count].getExlValue()).intValue());
							} else if (DataTypeEnum.BYTE.getValue().equalsIgnoreCase(evc[count].getExlColType())) {
								field.set(t, evc[count].getExlValue().getBytes(StandardCharsets.UTF_8));
							}

						} catch (IllegalArgumentException | IllegalAccessException excp) {
							excp.getStackTrace();
						}
						break;
					}
				}
			}
			list.add(t);
		});
		return list;
	}

}
