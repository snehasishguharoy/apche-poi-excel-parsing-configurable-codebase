package com.alekos.digital.reader;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import com.alekos.digital.cnst.DataTypeEnum;
import com.alekos.digital.config.ExcelValueCnfg;
import com.alekos.digital.excp.BsnObjExcp;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExcelReader {

	private ExcelReader() {

	}

	public static Workbook readExcl(final String fullFilePath)
			throws EncryptedDocumentException, IOException, BsnObjExcp {
		Workbook workbook = null;
		try {
			workbook = WorkbookFactory.create(new File(fullFilePath));
			
		} catch (InvalidFormatException excp) {
			throw new BsnObjExcp("Exception occurred while creating the file using the input file path");
		}
		return workbook;

	}

	private static List<Map<String, List<ExcelValueCnfg>>> getExclHeaderCnfgSections() throws BsnObjExcp {
		List<Map<String, List<ExcelValueCnfg>>> jsonMap = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			String jsonCnfg = new String(
					Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("excelconfig.json").toURI())));
			jsonMap = objectMapper.readValue(jsonCnfg,
					new TypeReference<List<HashMap<String, List<ExcelValueCnfg>>>>() {
					});
		} catch (IOException | URISyntaxException excp) {
			excp.printStackTrace();
		//	throw new BsnObjExcp("Exception occurred while reading the metadata json file");
		}
		return jsonMap;

	}

	private static Map<String, ExcelValueCnfg[]> getExcelHeaderSections() throws BsnObjExcp {
		List<Map<String, List<ExcelValueCnfg>>> jsonConfigMap = getExclHeaderCnfgSections();
		Map<String, ExcelValueCnfg[]> jsonMap = new HashMap<>();
		jsonConfigMap.forEach(jps -> {
			jps.forEach((section, values) -> {
				ExcelValueCnfg[] excelValueCnfgs = new ExcelValueCnfg[values.size()];
				jsonMap.put(section, values.toArray(excelValueCnfgs));
			});
		});
		return jsonMap;
	}

	public static Map<String, List<ExcelValueCnfg[]>> getExclRwValues(final Sheet sheet) throws BsnObjExcp {
		Map<String, List<ExcelValueCnfg[]>> exclMap = new HashMap<>();
		Map<String, ExcelValueCnfg[]> exlSectionHeaders = getExcelHeaderSections();
		Integer totalRws = sheet.getLastRowNum();
		System.out.println(totalRws);
		exlSectionHeaders.forEach((section, exlValuesCnfgs) -> {
			List<ExcelValueCnfg[]> valueCnfgs = new ArrayList<>();
			int count=0;
			for (int i = 1; i <=totalRws; i++) {
			//	System.out.println(totalRws);
				Row row = sheet.getRow(i);
				ExcelValueCnfg[] excelValueCnfgs = new ExcelValueCnfg[exlValuesCnfgs.length];
				int k = 0;
				for (ExcelValueCnfg excelValueCnfg : exlValuesCnfgs) {
					int cellIndex = excelValueCnfg.getExlIndex();
					String cellType = excelValueCnfg.getExlColType();
					Cell cell = row.getCell(cellIndex);
					ExcelValueCnfg valueCnfg = new ExcelValueCnfg();
					valueCnfg.setExlColType(excelValueCnfg.getExlColType());
					valueCnfg.setExlHeader(excelValueCnfg.getExlHeader());
					valueCnfg.setExlIndex(excelValueCnfg.getExlIndex());
					valueCnfg.setPojoProperty(excelValueCnfg.getPojoProperty());
					if (DataTypeEnum.STRING.getValue().equalsIgnoreCase(cellType)) {
				//		System.out.println(cell.getStringCellValue());
						valueCnfg.setExlValue(cell.getStringCellValue());
					} else if (DataTypeEnum.DOUBLE.getValue().equalsIgnoreCase(cellType)
							|| DataTypeEnum.INTEGER.getValue().equalsIgnoreCase(cellType)) {
						valueCnfg.setExlValue(String.valueOf(cell.getNumericCellValue()));
					}else if(DataTypeEnum.BYTE.getValue().equalsIgnoreCase(cellType)) {
					
						if(count<cell.getSheet().getWorkbook().getAllPictures().size() && (null!=cell.getSheet().getWorkbook().getAllPictures())){
						//	System.out.println(cell.getSheet().getWorkbook().getAllPictures().size());
					//	System.out.println((cell.getSheet().getWorkbook().getAllPictures().get(count)));
						String str=new String(cell.getSheet().getWorkbook().getAllPictures().get(count).getData());
						count++;
						valueCnfg.setExlValue(str);
					//	System.out.println(str);
						}
					}
					
					excelValueCnfgs[k++] = valueCnfg;
				}
				valueCnfgs.add(excelValueCnfgs);
				
			}
			exclMap.put(section, valueCnfgs);
		});
		return exclMap;
	}

}
