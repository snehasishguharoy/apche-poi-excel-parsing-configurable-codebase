package com.alekos.digital.excp;

public class BsnObjExcp extends Exception {

	private static final long serialVersionUID = 1L;

	public BsnObjExcp(String msg) {
		super(msg);
	}

}
