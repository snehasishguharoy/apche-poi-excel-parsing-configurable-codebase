package com.alekos.digital.main;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.alekos.digital.cnst.ExcelSectionEnum;
import com.alekos.digital.config.ExcelValueCnfg;
import com.alekos.digital.excp.BsnObjExcp;
import com.alekos.digital.mapper.ExcelPojoMapper;
import com.alekos.digital.reader.ExcelReader;
import com.alekos.digital.vo.Physics;

public class FileParsingMain {

	public static void main(String[] args) throws BsnObjExcp {
		try {
		Workbook workbook = ExcelReader.readExcl("C:\\Users\\sony\\Desktop\\Book2.xlsx");
			
		
			Sheet sheet = workbook.getSheetAt(0);
			Map<String, List<ExcelValueCnfg[]>> excelRowValuesMap = ExcelReader.getExclRwValues(sheet);
			List<Physics> physics = ExcelPojoMapper.getPojos(excelRowValuesMap.get(ExcelSectionEnum.PHYSICS.getValue()),
					Physics.class);
			physics.stream().forEach(System.out::println);
		//	System.out.println(physics.size());

		} catch (EncryptedDocumentException | IOException e) {
			e.printStackTrace();
		}
	}

}
