package com.alekos.digital.vo;

import java.util.Arrays;

public class Physics {
	
	private Integer questionNo;
	private String subject;
	private String chapter;
	private String subChapter;
	private String question;
	private byte [] optionA;
	private byte [] optionB;
	public byte[] getOptionA() {
		return optionA;
	}
	public byte[] getOptionB() {
		return optionB;
	}
	public void setOptionB(byte[] optionB) {
		this.optionB = optionB;
	}
	public void setOptionA(byte[] optionA) {
		this.optionA = optionA;
	}
	public Integer getQuestionNo() {
		return questionNo;
	}
	public void setQuestionNo(Integer questionNo) {
		this.questionNo = questionNo;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getChapter() {
		return chapter;
	}
	public void setChapter(String chapter) {
		this.chapter = chapter;
	}
	public String getSubChapter() {
		return subChapter;
	}
	public Physics() {
		super();
	}
	
	
	@Override
	public String toString() {
		return "Physics [questionNo=" + questionNo + ", subject=" + subject + ", chapter=" + chapter + ", subChapter="
				+ subChapter + ", question=" + question + ", optionA=" + Arrays.toString(optionA) + ", optionB="
				+ Arrays.toString(optionB) + "]";
	}
	public void setSubChapter(String subChapter) {
		this.subChapter = subChapter;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}

}
