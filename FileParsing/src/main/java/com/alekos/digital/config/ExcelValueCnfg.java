package com.alekos.digital.config;

public class ExcelValueCnfg {
	private String exlHeader;
	private Integer exlIndex;
	private String exlColType;
	private String exlValue;
	private String pojoProperty;
	public String getExlHeader() {
		return exlHeader;
	}
	public void setExlHeader(String exlHeader) {
		this.exlHeader = exlHeader;
	}
	public Integer getExlIndex() {
		return exlIndex;
	}
	public void setExlIndex(Integer exlIndex) {
		this.exlIndex = exlIndex;
	}
	public String getExlColType() {
		return exlColType;
	}
	public void setExlColType(String exlColType) {
		this.exlColType = exlColType;
	}
	public String getExlValue() {
		return exlValue;
	}
	public void setExlValue(String exlValue) {
		this.exlValue = exlValue;
	}
	public String getPojoProperty() {
		return pojoProperty;
	}
	@Override
	public String toString() {
		return "ExcelValueCnfg [exlHeader=" + exlHeader + ", exlIndex=" + exlIndex + ", exlColType=" + exlColType
				+ ", exlValue=" + exlValue + ", pojoProperty=" + pojoProperty + "]";
	}
	public void setPojoProperty(String pojoProperty) {
		this.pojoProperty = pojoProperty;
	}
	public ExcelValueCnfg() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
